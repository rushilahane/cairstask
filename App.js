import {Text, SafeAreaView, View} from 'react-native';
import React, {Component} from 'react';
import {AuthProvider} from './src/routes/AuthProvider';
import {Provider} from 'react-redux';
import Route from './src/routes/Route';
import configureStore from "./src/redux/store/configureStore";
const store = configureStore();
export class App extends Component {


  render() {
    return (
      <AuthProvider>
        <Provider store={store}>
          <SafeAreaView style={{flex: 1}}>
            <Route />
          </SafeAreaView>
          </Provider>
      </AuthProvider>
    );
  }
}

export default App;
