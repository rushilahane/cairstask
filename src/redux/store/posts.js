import {createSlice} from '@reduxjs/toolkit';
import {apiCallBegan} from './api';

const slice = createSlice({
  name: 'posts',
  initialState: {
    list: [],
    loading: false,
  },

  reducers: {
    postsRequested: (posts, action) => {
      posts.loading = true;
    },

    postsReceived: (posts, action) => {
      let temp = [];
      let tempData = action.payload.users;
      if (tempData.length != 0) {
        tempData.map((item, index) => {
          console.log('in', item);
          temp.push({value: item.username, key: item.id, image: item.image});
        });
        posts.list = temp;
        posts.loading = false;
      }
    },

    postsRequestFailed: (posts, action) => {
      posts.loading = false;
    },
  },
});

export default slice.reducer;

const {postsRequested, postsReceived, postsRequestFailed} = slice.actions;

const url = '/users';

export const loadposts = () => dispatch => {
  return dispatch(
    apiCallBegan({
      url,
      onStart: postsRequested.type,
      onSuccess: postsReceived.type,
      onError: postsRequestFailed.type,
    }),
  );
};
