import {
  View,
  Text,
  Image,
  StyleSheet,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {AppImages, Appstrings} from '../constant';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import {loadposts} from '../redux/store/posts';
import {AlphabetList} from 'react-native-section-alphabet-list';
const Dashbord = () => {
  const dispatch = useDispatch();
  const posts = useSelector(state => state);

  useEffect(() => {
    dispatch(loadposts());
  }, []);

  const exampleData = [{value: 'Afghanistan', key: 'AF'}];

  const logOutFunction = async () => {
    await AsyncStorage.removeItem(Appstrings.ACCESS_TOKEN);
    await AsyncStorage.removeItem(Appstrings.IS_LOGIN);
  };

  const renderListItem = item => {
    return (
      <View style={styles.listItemContainer}>
        <View
          style={{
            width: '10%',
            marginHorizontal: 10,
            resizeMode: 'contain',
            borderRadius: 10,
            overflow: 'hidden',
          }}>
          <Image
            source={{uri: item.image}}
            style={{
              width: '90%',

              height: '90%',
              resizeMode: 'contain',
            }}
          />
        </View>

        <Text style={styles.listItemLabel}>{item.value}</Text>
      </View>
    );
  };

  const renderSectionHeader = section => {
    return (
      <View style={styles.sectionHeaderContainer}>
        <Text style={styles.sectionHeaderLabel}>{section.title}</Text>
      </View>
    );
  };

  const headerFunction = () => {
    return (
      <View style={styles.headeView}>
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              'Are you sure want lo logout?',
              'This action will perform logout operation',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    logOutFunction();
                  },
                },
              ],
            );
          }}
          style={styles.backArrowView}>
          <Image source={AppImages.BACKARROW} style={styles.backarrow} />
        </TouchableOpacity>
        <View style={{width: '80%'}}>
          <Text style={styles.pageGHEad}>Follower</Text>
        </View>
      </View>
    );
  };

  const chooseViewFunction = () => {
    return (
      <View style={styles.listviewtype}>
        <View
          style={{
            width: '45%',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity>
            <Image
              source={AppImages.GRIDVER}
              style={{width: 25, height: 25, tintColor: '#6b6a6a'}}
            />
          </TouchableOpacity>
        </View>
        <View style={{width: '45%'}}>
          <TouchableOpacity>
            <Image
              source={AppImages.GRIDHORI}
              style={{width: 25, height: 25, tintColor: '#9c9c9c'}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const activityIndicatorShow = () => {
    return (
      <Modal visible={posts.loading} transparent={true}>
        <View style={styles.activityIndicatorView}>
          <ActivityIndicator size={50} color={'#df7352'} />
        </View>
      </Modal>
    );
  };
  return (
    <View style={styles.container}>
      {headerFunction()}
      {chooseViewFunction()}
      {activityIndicatorShow()}
      <AlphabetList
        style={styles.alphabetList}
        data={posts.list}
        renderCustomItem={renderListItem}
        renderCustomSectionHeader={renderSectionHeader}
        indexLetterStyle={{
          color: '#007aff',
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headeView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 10,
  },
  backArrowView: {
    width: '10%',
    alignItems: 'center',
  },
  backarrow: {width: 18, height: 18, tintColor: '#747678'},
  pageGHEad: {
    fontSize: 16,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontStyle: 'italic',
    color: '#747678',
  },
  listviewtype: {
    backgroundColor: '#d7dbe0',
    width: '100%',
    padding: 5,
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  alphabetList: {
    flex: 1,
  },

  listItemContainer: {
    flex: 1,
    height: 40,
    width: '85%',
    paddingHorizontal: 15,
    justifyContent: 'flex-start',
    borderTopColor: '#e6ebf2',
    alignItems: 'center',
    borderTopWidth: 1,
    alignSelf: 'center',
    flexDirection: 'row',
  },

  listItemLabel: {
    color: '#90979f',
    fontWeight: '800',
    fontSize: 14,
  },

  sectionHeaderContainer: {
    width: '10%',
    height: 30,
    position: 'absolute',
    justifyContent: 'center',
    marginHorizontal: 10,
    marginTop: 5,
  },

  sectionHeaderLabel: {
    fontSize: 20,
    width: '100%',
    alignSelf: 'center',

    color: '#6f757a',
    fontWeight: '800',
  },

  listHeaderContainer: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityIndicatorView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
});

export default Dashbord;
