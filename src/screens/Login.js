import {
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {AppImages, Appstrings} from '../constant';
import AsyncStorage from '@react-native-async-storage/async-storage';
Login = () => {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');

  const loginUser = async () => {
    if (email.length == 0) {
      alert('Please enter yuur email!');
      return;
    }
    if (password.length == 0) {
      alert('Please enter yuur password!');
      return;
    }
    await AsyncStorage.setItem(Appstrings.ACCESS_TOKEN, 'xyz');
    await AsyncStorage.setItem(Appstrings.IS_LOGIN, 'true');
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        <View style={[styles.helloCOntainer, {padding: 50}]}>
          <Text style={styles.hellowWord}>Hello</Text>
        </View>

        <View style={[styles.helloCOntainer, {paddingTop: 50}]}>
          <Text style={styles.welcomTYext}>Welcome!</Text>
        </View>
        <View style={styles.textInputCOntainer}>
          <TextInput
            value={email}
            style={styles.textInputView}
            placeholder="Email"
            placeholderTextColor={'#b8b9ba'}
            keyboardType={'email-address'}
            onChangeText={text => {
              setemail(text);
            }}
          />
          <TextInput
            value={password}
            style={styles.textInputView}
            placeholder="Password"
            placeholderTextColor={'#b8b9ba'}
            secureTextEntry={true}
            onChangeText={text => {
              setpassword(text);
            }}
          />

          <TouchableOpacity
            onPress={() => loginUser()}
            style={[
              styles.textInputView,
              {
                backgroundColor: '#df7352',
                marginTop: 20,
              },
            ]}>
            <Text style={styles.loginTExt}>Login</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.helloCOntainer, {padding: 40}]}>
          <TouchableOpacity>
            <Text style={styles.forgotpassText}>Forgot password?</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.socialView}>
          <TouchableOpacity style={styles.socialButton}>
            <Image source={AppImages.GOGGLE} style={{width: 25, height: 25}} />
            <Text style={[styles.googleText, {color: '#df7352'}]}>Google</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.socialButton}>
            <Image
              source={AppImages.FACEBOOK}
              style={{width: 25, height: 25}}
            />
            <Text style={[styles.googleText, {color: '#0c3587'}]}>
              facebook
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.helloCOntainer, {padding: 40}]}>
          <TouchableOpacity>
            <Text style={styles.forgotpassText}>
              <Text style={{color: '#a9aaab'}}>
                Don't have an account?{'\t'}
              </Text>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  helloCOntainer: {width: '100%'},
  hellowWord: {
    width: '90%',
    alignSelf: 'center',
    fontSize: 18,
    textAlign: 'center',
    fontStyle: 'italic',
    fontWeight: '600',
    color: '#df7352',
  },
  welcomTYext: {
    fontSize: 18,
    width: '90%',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  textInputCOntainer: {
    width: '100%',
    alignItems: 'center',
  },
  textInputView: {
    width: '90%',
    backgroundColor: '#dedfe0',
    borderColor: '#dedfe0',
    borderWidth: 0.5,
    padding: 10,
    borderRadius: 15,
    marginTop: 10,
  },
  loginTExt: {
    fontSize: 16,
    color: 'white',
    fontWeight: '700',
    alignSelf: 'center',
  },
  forgotpassText: {
    fontSize: 14,
    alignSelf: 'center',
    fontWeight: '600',
    color: '#df7352',
  },
  socialButton: {
    width: '47.5%',
    padding: 10,
    borderWidth: 0.5,
    borderColor: '#919294',
    borderRadius: 12,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  socialView: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  googleText: {alignSelf: 'center', fontSize: 14, right:12, fontWeight: '600'},
});

export default Login;
