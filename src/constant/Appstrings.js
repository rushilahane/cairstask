const Appstrings = {
    ACCESS_TOKEN: 'access_token',
    USER_NAME: 'user_name',
    USER_EMAIL: 'user_email',
    USER_PHONE_NUMBER: 'phone-number',
    LOGIN_TYPE: 'logintype',
    DEVICE_TYPE: 'devicetype',
    DEVICE_TOKEN: 'device-token',
    USER_TOKEN: 'userToken',
    USER_IMAGE: 'USERIMG',
    USER_ID: 'userid',
    IS_LOGIN: 'islogin',
  };
  export default Appstrings;
  