const AppImages={
 GOGGLE:require("../assets/images/google.png"),
 FACEBOOK:require("../assets/images/facebook.png"),
 BACKARROW:require("../assets/images/backarrow.png"),
 GRIDHORI:require("../assets/images/grid.png"),
 GRIDVER:require("../assets/images/gridVeticle.png")
}
export default AppImages;