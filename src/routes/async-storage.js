import AsyncStorage from '@react-native-async-storage/async-storage';
import {Appstrings} from '../constant/index';
export async function getItem() {
  const value = await AsyncStorage.getItem(Appstrings.IS_LOGIN);

  return value ? value : null;
}

export async function setItem(value) {
  return AsyncStorage.setItem(Appstrings.ACCESS_TOKEN, value);
}
export async function removeItem() {
  return AsyncStorage.removeItem(Appstrings.ACCESS_TOKEN);
}
