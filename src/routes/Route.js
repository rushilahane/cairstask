import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Dashbord from '../screens/Dashbord';
import Login from '../screens/Login';
const Stack = createNativeStackNavigator();

import {useAuthorization} from './AuthProvider';

export default function Route() {
  const {status, authToken} = useAuthorization();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {authToken == null ? (
          <Stack.Screen
            name="Auth"
            component={AuthStack}
            header={null}
            options={{headerShown: false}}
          />
        ) : (
          <Stack.Screen
            name="Dashboard"
            component={DashboardStackScreen}
            header={null}
            options={{headerShown: false}}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function AuthStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        header={null}
        options={{headerShown: false, presentation: 'modal'}}
      />
    </Stack.Navigator>
  );
}
const DashboardStackScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Dashbord"
        component={Dashbord}
        header={null}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
